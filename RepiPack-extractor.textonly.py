#!/usr/bin/env python

import sys, os, hashlib, binascii
import md5py
import lzss
from array import array
from cStringIO import StringIO
from insani import *

if len(sys.argv) <> 3:
    print 'Please give an archive filename and a desired output directory on the\ncommand line.'
    sys.exit(0)

# https://gist.github.com/c633/a7a5cde5ce1b679d3c0a
max_bits = 32  # For fun, try 2, 17 or other arbitrary (positive!) values

# Rotate left: 0b1001 --> 0b0011
rol = lambda val, r_bits, max_bits: \
    (val << r_bits%max_bits) & (2**max_bits-1) | \
    ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))
 
# Rotate right: 0b1001 --> 0b1100
ror = lambda val, r_bits, max_bits: \
    ((val & (2**max_bits-1)) >> r_bits%max_bits) | \
    (val << (max_bits-(r_bits%max_bits)) & (2**max_bits-1))

# http://stackoverflow.com/a/27843760
def reversed_string(a_string):
    return a_string[::-1]

def decrypt(indata, xorkey):
    i = 0
    while (i + 3) < len(indata):
        integer = indata[i+3] << 24 | indata[i+2] << 16 | indata[i+1] << 8 | indata[i]
        integer ^= xorkey
        indata[i] = integer & 0xFF
        indata[i+1] = (integer >> 8) & 0xFF
        indata[i+2] = (integer >> 16) & 0xFF
        indata[i+3] = (integer >> 24) & 0xFF
        integer = rol(integer, 16, max_bits)
        integer ^= 0x77DF8518
        xorkey += integer
        i += 4
    return indata

def read_entry(infile):
    result = {}
    fileentry = []
    for i in xrange(32):
        fileentry.append(read_unsigned(infile, BYTE_LENGTH))
    fileentry = decrypt(fileentry, 0xF358ACE6)

    filenamemd5 = []
    for i in xrange(16):
        filenamemd5.append(fileentry[i])
    result["filenamemd5"] = ''.join('{:02x}'.format(e) for e in filenamemd5)
    result["fileoffset"] = fileentry[16+3] << 24 | fileentry[16+2] << 16 | fileentry[16+1] << 8 | fileentry[16+0]
    result["filesize"] = fileentry[20+3] << 24 | fileentry[20+2] << 16 | fileentry[20+1] << 8 | fileentry[20+0]
    result["origsize"] = fileentry[24+3] << 24 | fileentry[24+2] << 16 | fileentry[24+1] << 8 | fileentry[24+0]
    result["unknown"] = fileentry[28+3] << 24 | fileentry[28+2] << 16 | fileentry[28+1] << 8 | fileentry[28+0]
    return result

def jisarray_properlower(chararray):
    i = 0
    while i < len(chararray):
        if chararray[i] >= 0x00 and chararray[i] <= 0x40:
            i += 1
        elif chararray[i] >= 0x41 and chararray[i] <= 0x5A:
            chararray[i] += 0x20
            i += 1
        elif chararray[i] >= 0x5B and chararray[i] <= 0x7F:
            i += 1
        elif chararray[i] >= 0x80 and chararray[i] <= 0xA0:
            i += 2
        elif chararray[i] >= 0xA1 and chararray[i] <= 0xDF:
            i += 1
        elif chararray[i] >= 0xE0 and chararray[i] <= 0xFF:
            i += 2
    return chararray

arcfile = open(sys.argv[1], 'rb')
dirname = sys.argv[2]
assert_string(arcfile, 'RepiPack', ERROR_ABORT)
arcver = read_unsigned(arcfile)
print "Archive version:", hex(arcver)
arcnamelen = read_unsigned(arcfile)
arcname = []
for i in xrange(arcnamelen):
    arcname.append(read_unsigned(arcfile, BYTE_LENGTH))
arcname = decrypt(arcname, 0xBD42D5EF)
arcname = ''.join(chr(e) for e in arcname)
print "Archive name:", arcname

filenum = read_unsigned(arcfile)
print "# of files inside:", filenum

# Build the filenames array
namesarray = []
with open("filenames.txt") as f:
    for line in f:
        namemd5entry = {}
        namemd5entry["name"] = line[:-1]
        namemd5 = hashlib.md5()
        chararray = array('B', namemd5entry["name"])
        chararray = jisarray_properlower(chararray)
        namemd5.update(chararray.tostring())
        namemd5entry["md5"] = namemd5.hexdigest()
        namesarray.append(namemd5entry)
f.close()

indexarray = []
for i in xrange(filenum):
    fileentry = read_entry(arcfile)
    indexarray.append(fileentry)

for elem in xrange(len(indexarray)):
    name = 0
    namefound = 0
    filename = indexarray[elem]['filenamemd5']
    while name < len(namesarray) and namefound == 0:
        if namesarray[name]['md5'] == indexarray[elem]['filenamemd5']:
            filename = namesarray[name]['name']
            namefound = 1
        else:
            name += 1

    if filename.endswith('.txt') or filename.endswith('.def') or filename.endswith('.inc') or filename.endswith('.flg') or filename.endswith('.ffd') or filename.endswith('.ssc'):
        # print 'Extracting %s (%d -> %d bytes)' % (filename.decode('shift-jis').encode('utf-8'), indexarray[elem]['filesize'], indexarray[elem]['origsize'])
        pathcomponents = filename.decode('shift-jis').split(u'/')

        filepath = dirname
        for direct in pathcomponents:
            if not os.path.isdir(filepath.decode('utf-8')):  # Create directory if it's not there
                os.mkdir(filepath.decode('utf-8'))           # Won't do this for the final filename
            filepath = os.path.join(filepath, direct.encode('utf-8'))

        chararray = array('B', filename)
        chararray = jisarray_properlower(chararray)
        filenamereversed = reversed_string(chararray.tostring())
        onetimepad = []
        # print "Building decryption block..."
        hashkey = md5py.new()
        for round in xrange(64):
            hashkey.update(filenamereversed)
            digest = hashkey.digest()
            for md5byte in xrange(len(digest)):
                onetimepad.append(ord(digest[md5byte]))
            filenamereversed = filenamereversed[1:]
            if len(filenamereversed) == 0:
                chararray = array('B', filename)
                chararray = jisarray_properlower(chararray)
                filenamereversed = reversed_string(chararray.tostring())

        arcfile.seek(indexarray[elem]['fileoffset'], 0)
        data = arcfile.read(indexarray[elem]['filesize'])

        outbuffer = StringIO()
        outbuffer.write(data)
        outbuffer.seek(0)
        tmpbuf = array("B", outbuffer.read())
        # print "Decrypting..."
        j = 0
        if len(tmpbuf) <= 0x400:
            xorrange = len(tmpbuf)
        else:
            xorrange = 0x400
        for filebyte in xrange(xorrange):
            tmpbuf[filebyte] ^= onetimepad[j]
            j += 1
        outbuffer.seek(0)
        outbuffer.write(tmpbuf.tostring())
        data = outbuffer.getvalue()
        if indexarray[elem]['filesize'] <> indexarray[elem]['origsize']:
            # print "Decompressing..."
            data = lzss.decode(tmpbuf,0,len(tmpbuf))
        outbuffer.truncate(0)
        outbuffer.seek(0)
        outbuffer.write(data)
        outbuffer.seek(0)

        outfile = open(filepath.decode('utf-8'), "wb")
        outfile.write(outbuffer.getvalue())
        outfile.close

arcfile.close()
