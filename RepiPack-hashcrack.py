#!/usr/bin/env python

import sys, os, hashlib, binascii
from array import array

if len(sys.argv) <> 3:
    print 'Please supply the name of the wordlist and the hashlist file.'
    sys.exit(0)

def jisarray_properlower(chararray):
    i = 0
    while i < len(chararray):
        if chararray[i] >= 0x00 and chararray[i] <= 0x40:
            i += 1
        elif chararray[i] >= 0x41 and chararray[i] <= 0x5A:
            chararray[i] += 0x20
            i += 1
        elif chararray[i] >= 0x5B and chararray[i] <= 0x7F:
            i += 1
        elif chararray[i] >= 0x80 and chararray[i] <= 0xA0:
            i += 2
        elif chararray[i] >= 0xA1 and chararray[i] <= 0xDF:
            i += 1
        elif chararray[i] >= 0xE0 and chararray[i] <= 0xFF:
            i += 2
    return chararray

# Build the filenames array
namesarray = []
namesmd5array = []
with open(sys.argv[0x0001]) as f:
    for line in f:
        namesarray.append(line[:-1])
        chararray = array('B', line[:-1])
        chararray = jisarray_properlower(chararray)
        namemd5 = hashlib.md5()
        namemd5.update(chararray.tostring())
        namesmd5array.append(namemd5.digest())
f.close()

# Build the hashes array
hashes = []
with open(sys.argv[0x0002]) as f:
    for line in f:
        hashes.append(binascii.unhexlify(line[:-1]))
f.close()

for hash in xrange(len(hashes)):
    cracked = 0
    if hashes[hash] in namesmd5array:
        pos = namesmd5array.index(hashes[hash])
        cracked = 1
    if cracked == 1:
        print namesarray[pos]
    else:
        print binascii.hexlify(hashes[hash])
