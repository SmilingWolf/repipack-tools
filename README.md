# RepiPack Tools

Python tools to work with RepiPack files such as the ones used by Littlewitch Romanesque.  
I had to two goals in mind when creating these:

* the extracted files should, wherever possible, be extracted with the filename using the right case (pet peeve of mine)
* the script must be able to unpack and repack ALL the files, not only the ones we could decrypt in the first place

So yeah, there you have it. This script does exactly that.