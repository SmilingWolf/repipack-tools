#!/usr/bin/env python

import sys, os, hashlib, binascii
import md5py
import lzss
from array import array
from cStringIO import StringIO
from insani import *

if len(sys.argv) <> 2:
    print 'Please give an archive filename.'
    sys.exit(0)

# https://gist.github.com/c633/a7a5cde5ce1b679d3c0a
max_bits = 32  # For fun, try 2, 17 or other arbitrary (positive!) values

# Rotate left: 0b1001 --> 0b0011
rol = lambda val, r_bits, max_bits: \
    (val << r_bits%max_bits) & (2**max_bits-1) | \
    ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))
 
# Rotate right: 0b1001 --> 0b1100
ror = lambda val, r_bits, max_bits: \
    ((val & (2**max_bits-1)) >> r_bits%max_bits) | \
    (val << (max_bits-(r_bits%max_bits)) & (2**max_bits-1))

# http://stackoverflow.com/a/27843760
def reversed_string(a_string):
    return a_string[::-1]

def decrypt(indata, xorkey):
    i = 0
    while (i + 3) < len(indata):
        integer = indata[i+3] << 24 | indata[i+2] << 16 | indata[i+1] << 8 | indata[i]
        integer ^= xorkey
        indata[i] = integer & 0xFF
        indata[i+1] = (integer >> 8) & 0xFF
        indata[i+2] = (integer >> 16) & 0xFF
        indata[i+3] = (integer >> 24) & 0xFF
        integer = rol(integer, 16, max_bits)
        integer ^= 0x77DF8518
        xorkey += integer
        i += 4
    return indata

def read_entry(infile):
    result = {}
    fileentry = []
    for i in xrange(32):
        fileentry.append(read_unsigned(infile, BYTE_LENGTH))
    fileentry = decrypt(fileentry, 0xF358ACE6)

    filenamemd5 = []
    for i in xrange(16):
        filenamemd5.append(fileentry[i])
    result["filenamemd5"] = ''.join('{:02x}'.format(e) for e in filenamemd5)
    result["fileoffset"] = fileentry[16+3] << 24 | fileentry[16+2] << 16 | fileentry[16+1] << 8 | fileentry[16+0]
    result["filesize"] = fileentry[20+3] << 24 | fileentry[20+2] << 16 | fileentry[20+1] << 8 | fileentry[20+0]
    result["origsize"] = fileentry[24+3] << 24 | fileentry[24+2] << 16 | fileentry[24+1] << 8 | fileentry[24+0]
    result["unknown"] = fileentry[28+3] << 24 | fileentry[28+2] << 16 | fileentry[28+1] << 8 | fileentry[28+0]
    return result

arcfile = open(sys.argv[0x0001], 'rb')
assert_string(arcfile, 'RepiPack', ERROR_ABORT)
arcver = read_unsigned(arcfile)
arcnamelen = read_unsigned(arcfile)
arcname = []
for i in xrange(arcnamelen):
    arcname.append(read_unsigned(arcfile, BYTE_LENGTH))
arcname = decrypt(arcname, 0xBD42D5EF)

filenum = read_unsigned(arcfile)

indexarray = []
for i in xrange(filenum):
    fileentry = read_entry(arcfile)
    indexarray.append(fileentry)

for elem in xrange(len(indexarray)):
    print indexarray[elem]["filenamemd5"]

arcfile.close()
