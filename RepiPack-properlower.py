#!/usr/bin/env python

import sys, os, hashlib, binascii
import md5py
import lzss
from array import array
from cStringIO import StringIO
from insani import *

if len(sys.argv) <> 1:
    print 'This script uses no arguments.'
    sys.exit(0)

def jisarray_properlower(chararray):
    i = 0
    while i < len(chararray):
        if chararray[i] >= 0x00 and chararray[i] <= 0x40:
            i += 1
        elif chararray[i] >= 0x41 and chararray[i] <= 0x5A:
            chararray[i] += 0x20
            i += 1
        elif chararray[i] >= 0x5B and chararray[i] <= 0x7F:
            i += 1
        elif chararray[i] >= 0x80 and chararray[i] <= 0xA0:
            i += 2
        elif chararray[i] >= 0xA1 and chararray[i] <= 0xDF:
            i += 1
        elif chararray[i] >= 0xE0 and chararray[i] <= 0xFF:
            i += 2
    return chararray

# Build the filenames array
with open("filenames.txt") as f:
    for line in f:
        chararray = array('B', line[:-1])
        chararray = jisarray_properlower(chararray)
        print chararray.tostring()
f.close()
