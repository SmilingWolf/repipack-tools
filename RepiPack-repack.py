#!/usr/bin/env python

import sys, os, hashlib, binascii
import md5py
import lzss
from stat import *
from array import array
from cStringIO import StringIO
from insani import *

if len(sys.argv) <> 3:
    print 'Please give a directory name and a desired output archive on the\ncommand line.'
    sys.exit(0)

# https://gist.github.com/c633/a7a5cde5ce1b679d3c0a
max_bits = 32  # For fun, try 2, 17 or other arbitrary (positive!) values

# Rotate left: 0b1001 --> 0b0011
rol = lambda val, r_bits, max_bits: \
    (val << r_bits%max_bits) & (2**max_bits-1) | \
    ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))
 
# Rotate right: 0b1001 --> 0b1100
ror = lambda val, r_bits, max_bits: \
    ((val & (2**max_bits-1)) >> r_bits%max_bits) | \
    (val << (max_bits-(r_bits%max_bits)) & (2**max_bits-1))

# http://stackoverflow.com/a/27843760
def reversed_string(a_string):
    return a_string[::-1]

# http://stackoverflow.com/a/234329
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def encrypt(indata, xorkey):
    i = 0
    while (i + 3) < len(indata):
        clear = indata[i+3] << 24 | indata[i+2] << 16 | indata[i+1] << 8 | indata[i]
        crypt = clear ^ xorkey
        indata[i] = crypt & 0xFF
        indata[i+1] = (crypt >> 8) & 0xFF
        indata[i+2] = (crypt >> 16) & 0xFF
        indata[i+3] = (crypt >> 24) & 0xFF
        clear = rol(clear, 16, max_bits)
        clear ^= 0x77DF8518
        xorkey += clear
        i += 4
    return indata
    
def long2array(long):
    bytearray = array('B')
    bytearray.append((long >> 0) & 0xFF)
    bytearray.append((long >> 8) & 0xFF)
    bytearray.append((long >> 16) & 0xFF)
    bytearray.append((long >> 24) & 0xFF)
    return bytearray

def write_entry(outbuffer, entry):
    entryarray = array('B', entry['md5'])
    entryarray += long2array(entry['fileoffset'])
    entryarray += long2array(entry['filesize'])
    entryarray += long2array(entry['origsize'])
    entryarray += long2array(entry['unknown'])
    entryarray = encrypt(entryarray, 0xF358ACE6)
    outbuffer.write(entryarray.tostring())

def jisarray_properlower(chararray):
    i = 0
    while i < len(chararray):
        if chararray[i] >= 0x00 and chararray[i] <= 0x40:
            i += 1
        elif chararray[i] >= 0x41 and chararray[i] <= 0x5A:
            chararray[i] += 0x20
            i += 1
        elif chararray[i] >= 0x5B and chararray[i] <= 0x7F:
            i += 1
        elif chararray[i] >= 0x80 and chararray[i] <= 0xA0:
            i += 2
        elif chararray[i] >= 0xA1 and chararray[i] <= 0xDF:
            i += 1
        elif chararray[i] >= 0xE0 and chararray[i] <= 0xFF:
            i += 2
    return chararray

dirname = sys.argv[1]
dirname = unicode(dirname, 'utf-8')
arcfile = open(sys.argv[2], 'wb')

# Write header
arcfile.write('RepiPack')
write_unsigned(arcfile, 5) # Archive version

arcname = array('B', sys.argv[2])
arcname = encrypt(arcname, 0xBD42D5EF)
arcname = ''.join(chr(e) for e in arcname)

write_unsigned(arcfile, len(arcname))
arcfile.write(arcname)

filesarray = []
for (dirpath, dirs, filenames) in walklevel(dirname, 1):
    for filename in filenames:
        fileentry = {}
        filepath = dirpath + '\\' + filename
        if S_ISREG(os.stat(filepath).st_mode):
            fileentry['filepath'] = filepath
            fileentry['filename'] = filename
            filesarray.append(fileentry)

write_unsigned(arcfile, len(filesarray))
write_zeroes(arcfile, len(filesarray)*32)

indexbuffer = StringIO()
for file in xrange(len(filesarray)):
    filename = filesarray[file]['filename'].encode('shift-jis')
    if not filename.endswith('-rawcopy'):
        # print "Building encryption block..."
        chararray = array('B', filename)
        chararray = jisarray_properlower(chararray)
        filenamereversed = reversed_string(chararray.tostring())
        onetimepad = []
        hashkey = md5py.new()
        for round in xrange(64):
            hashkey.update(filenamereversed)
            digest = hashkey.digest()
            for md5byte in xrange(len(digest)):
                onetimepad.append(ord(digest[md5byte]))
            filenamereversed = filenamereversed[1:]
            if len(filenamereversed) == 0:
                chararray = array('B', filename)
                chararray = jisarray_properlower(chararray)
                filenamereversed = reversed_string(chararray.tostring())
    infile = open(filesarray[file]['filepath'], 'rb')
    tmpbuf = array("B", infile.read())
    infile.close()
    if not filename.endswith('-rawcopy'):
        # print "Encrypting..."
        if len(tmpbuf) <= 0x400:
            xorrange = len(tmpbuf)
        else:
            xorrange = 0x400
        for filebyte in xrange(xorrange):
            tmpbuf[filebyte] ^= onetimepad[filebyte]

    outbuffer = StringIO()
    outbuffer.write(tmpbuf.tostring())

    # print "Creating the index entry..."
    indexentry = {}
    namemd5 = hashlib.md5()
    chararray = array('B', filename)
    chararray = jisarray_properlower(chararray)
    namemd5.update(chararray.tostring())
    indexentry['md5'] = namemd5.digest()
    indexentry['fileoffset'] = arcfile.tell()
    indexentry["filesize"] = indexentry["origsize"] = outbuffer.tell()
    indexentry["unknown"] = 0
    write_entry(indexbuffer, indexentry)
    print 'Storing %s (%d bytes)' % \
            (filename, indexentry["filesize"])
    arcfile.write(outbuffer.getvalue())

arcfile.seek(len(arcname)+20)
arcfile.write(indexbuffer.getvalue())
arcfile.close()
